<?php
//#require_once("svn_updater.php");
include_once "../settings.php";
?>
<!DOCTYPE html> 
<html> 
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta http-equiv="Content-Language" content="en-us" /> 
		<meta name="Description" content="ScriptPlayer"> 
		<title><?php echo $companyName; ?> :: AGC</title> 
		<link href="../bootstrap.min.css" rel="stylesheet" type="text/css" /> 
		<link href="styles/style.css" rel="stylesheet" type="text/css" /> 
		<script type="text/javascript" src="js/mootools.js"></script> 
		<script type="text/javascript" src="js/mootools-more.js"></script>
		<script type="text/javascript" src="js/Revocalize.js"></script>
		<script type="text/javascript" src="js/Revocalize.Player.js"></script>
		<script type="text/javascript">
			Revocalize.Config = <?php echo json_encode(array(
				'rep_id' => $_SESSION['user_id']
			))?>;
		</script>
	</head> 
	<body> 
		<div class="container-fluid">
			<div id="keyArea">
				<h1>Enter Key</h1>
				<form id="keyForm"> 
					<input type="text" id="keyPad" autocomplete="off" class="form-control"/>
				</form> 
				<?php if(!empty($_SESSION['user_id'])){ ?> <div id="logout">Howdy, <?php echo $_SESSION['userid']; ?><a 
href="logout.php"  class="btn btn-info" title="logout">Logout</a></div><?php } ?>
			</div> 
			<div id="hotkeys" class="grid-tile"></div> 
			
			<div id="script" class="grid-tile"></div> 
		</div>
	</body> 
</html>
