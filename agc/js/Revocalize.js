Element.implement({
	fixedCenter: function()
	{
		var size = this.getSize();
			w = Math.floor(size.x/2),
			h = Math.floor(size.y/2);
		this.setStyles({
			position: 'fixed',
			marginTop: '-' + h + 'px',
			marginLeft: '-' + w + 'px',
			top: '50%',
			left: '50%'
		});
	}
});
String.implement({
	escapeHTML: function()
	{
		return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
	}
});
Function.prototype.bind = function(scope)
{
	var that = this,
		args = $A(arguments).filter(function(v , i) {
			return (i !== 0);
		});;
	return function() {
		return that.apply(scope , args.extend($A(arguments)));
	};
}; // implements doesn't work, cause it protects bind, however MooTools bind doesn't let me curry :|

Revocalize = new Class({ // Generic methods that can be used anywhere
	Implements: [Events, Options],
	options: {
		scrimOverlayTemplate: '<div class="scrim-overlay {className}"></div>',
		errorDialogTemplate: '<div class="error-text">{text}</div>',
	},
	
	initialize: function(o)
	{
		o = o || {};
		this.setOptions(o);
	},
	
	getTimestamp: function()
	{
		return new Date().getTime();
	},
	
	showScrim: function()
	{
		if (!this.scrim)
		{
			this.scrim = new Element('div' , {"class": "scrim"});
			this.scrim.inject(this.body);
		}
		this.scrim.show();
	},
	
	hideScrim: function()
	{
		if (this.scrim)
		{
			this.scrim.hide();
		}
	},
	
	showScrimOverlay: function(o)
	{
		this.showScrim();
		o = o || {};
		if (!this.scrimOverlay)
		{
			this.scrimOverlay = Elements.from(this.options.scrimOverlayTemplate.substitute(o))[0];
			this.scrimOverlay.inject(this.body);
		}
		else
		{
			this.scrimOverlay = Elements.from(this.options.scrimOverlayTemplate.substitute(o))[0].replaces(this.scrimOverlay);
		}
		
		if ($type(o.html) == "string")
		{
			this.scrimOverlay.set("html" , o.html);
		}
		else
		{
			this.scrimOverlay.adopt(o.html);
		}
		this.scrimOverlay.fixedCenter();
	},
	
	hideScrimOverlay: function()
	{
		if (this.scrimOverlay)
		{
			this.scrimOverlay.destroy();
			delete this.scrimOverlay;
			this.hideScrim();
		}
	},
	
	showLoading: function(text)
	{
		text = text || "Loading...";
		this.showScrim();
		var label;
		if (!this.scrimLoading)
		{
			this.scrimLoading = new Element('div' , {"class": "scrim-loading"});
			this.scrimLoading.inject(this.body);
			label = new Element('label');
			label.inject(this.scrimLoading);
		}
		else
		{
			label = this.scrimLoading.getElement('label');
		}
		label.set("html" , text);
		if (text.substr(-3) == "...")
		{
			label.addClass('ellipsis');
		}
		else
		{
			label.removeClass('ellipsis');
		}
		this.scrimLoading.fixedCenter();
	},
	
	hideLoading: function()
	{
		this.hideScrim();
		if (this.scrimLoading)
		{
			this.scrimLoading.destroy();
			delete this.scrimLoading;
		}
	},
	
	errorDialog: function(text)
	{
		this.showScrimOverlay({html: Elements.from(this.options.errorDialogTemplate.substitute({text: text})) , className: "error"});
	},
	
	playAudio: function(file , callBack)
	{
		callBack = callBack || function(){};
		if (this.iframe)
		{
			this.stopAudio();
		}
		
		this.iframe = new Element('iframe' , {src: this.options.iframeHelperURL}).addClass('audio-iframe');
		var that = this; // gotta love closures. seriously.
		this.iframe.addEvent('load' , function(evt) {
			var audio = that.getiFrameAudioTag();
			audio.setAttribute('src' , file);
			audio.play();
			audio.addEventListener('ended' , callBack , true);
		});
		this.iframe.inject(this.body);
		//document.body.innerHTML();
	},
	
	stopAudio: function()
	{
		if (this.iframe)
		{
			var audio = this.getiFrameAudioTag();
			if (audio)
			{
				audio.pause();
			}
			this.iframe.destroy();
			delete this.iframe;
		}
	},
	
	getiFrameAudioTag: function()
	{
		if (this.iframe && this.iframe.contentDocument && this.iframe.contentDocument.getElementById)
		{
			return this.iframe.contentDocument.getElementById('audiotag');
		}
		else
		{
			return false;
		}
	}
});
