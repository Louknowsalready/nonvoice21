<!DOCTYPE html>
<html>
<body>

<audio controls loop id="music" controlsList="nodownload">
  <source src="sound4.ogg" type="audio/ogg">
  <source src="sound4.mp3" type="audio/mpeg">

</audio>
<br>
<strong>Volume:</strong><br>
<input id="vol-control" type="range" min="0" max="100" step="1" oninput="SetVolume(this.value)" onchange="SetVolume(this.value)">



<script>
	
window.SetVolume = function(val)
{
    var player = document.getElementById('music');
    console.log('Before: ' + player.volume);
    player.volume = val / 100;
    console.log('After: ' + player.volume);
}

</script>
</body>
</html>
