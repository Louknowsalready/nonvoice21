/*
 * Author : Joseph Ryan Cabatingan
 * Author URL: http://www.purpleconey.com
 */
$("document").ready(function(){
	//$(".scriptdetail:eq(0)").slideDown('fast');
	function toptitle(){
		$(".toptitle").click(function(){
			$(".scriptdetail").css("display","none");
			$(this).next(".scriptdetail").slideDown('fast');
		});
	}
	$("form[name=scriptform]").submit(function(){
		$("input[type=file]").each(function(){
			if($(this).attr("value")==""){
			//$(this).attr("disabled","disabled");
			}
		});
	});
	$(".scriptdetail h2").click(function(){
		$(".h3_response").css("display","none");
		$(this).next(".h3_response").slideDown("fast");
	});
	function setscroll(){
		$(".edit").click(function(){
			$(".detail").css("display","none");
			$(this).closest("h3").next(".detail").slideDown("fast");
		});
	}
	toptitle();
	setscroll();
	setdone();
	setcancel();
	$(".add_new").click(function(){
		$(".detail").css("display","none");
		origtb=$(this).closest("div").prev(".hidden_div");
		clonetb=origtb.clone();
		if(clonetb.find(".scriptdetail").size()>0){
		$(".scriptdetail").css("display","none");
			clonetb.find(".scriptdetail").css("display","block");
		}
		clonetb.html(clonetb.html().replace(/hidden_/gi,""));
		$(clonetb.html()).insertBefore(origtb);
		setokay();
		setdone();
		setcancel();
	});
	function setcancel(){
		$(".cancel").click(function(){
			if($(this).closest(".adnode").size()==0){
				a=$(this).closest("table");
				a.prev("h3").remove();
				a.remove();
			}else{
				a=$(this).closest("div");
				a.prev("h2").remove();
				a.remove();
			}
		});
		$(".hkcancel").click(function(){
                    	a=$(this).closest("table");
                        a.prev("h3").css("display","none");
			a.find("input[name*=hotkey_keycode]").attr("value","");
                        a.css("display","none");
                });
	}
	function setokay(){
		$(".okaysave").click(function(){
			if($(this).closest(".adnode").size()==0){
				a=$(this).closest("table");
				newtxt=a.find("input:eq(0)").val()+". "+a.find("textarea:eq(0)").val();
				a.prev("h3").html(a.prev("h3").html().replace("New",newtxt));
				$(".detail").css("display","none");
				setscroll();
				$(this).next(".cancel").attr("value","Delete");
				$(this).attr({value:"Done",class:"done"});
			}else{
				document.scriptform.submit();
			}
		});
	}
	$(".discard").click(function(){
		ds=document.scriptform;
		ds.action="removedraft.php";
		ds.submit();
	});
	function setdone(){
		$(".done").click(function(){
			if($(this).closest(".adnode").size()==0){
				$(".detail").css("display","none");
			}else{
				$(".scriptdetail").css("display","none");
			}
		});
	}
	$(".golive").click(function(){
		ds=document.scriptform;
		ds.golive.value=1;
		ds.submit();
	});
	
	$("i").each(function(){
		if($(this).find("audio").size()>0){
		  $(this).click(function(){
		  $(this).find("audio").attr("id","tempaudio");
		  document.getElementById("tempaudio").play();
		  $(this).find("audio").removeAttr("id");
		  }).css("cursor","pointer");
		}
	});
	$(".expand").click(function(){
		$(".detail").slideDown();
		$(".scriptdetail").slideDown();
	});
	$(".collapse").click(function(){
		$(".detail").slideUp();
		$(".scriptdetail").slideUp();
	});
});		
	function opacify(){
		$("#wrapper").css({"opacity":".1","filter":"alpha(opacity=10)"});
		$("body").append('<div class="waittxt">Please Wait...</div>');
	}
	function reopacify(){
		$("#wrapper").css({"opacity":"1","filter":"alpha(opacity=100)"});
		$(".waittxt").remove();
	}
