<?php

exec("chmod -R 0777 ../scripts");
require_once('../settings.php');
require_once('include_admin.php');
include('check.php');
?>
<script src="rwm.js" type="text/javascript" charset="utf-8"></script>
<form method="post" name="scriptform" action="/nonvoice/admin/savescript.php" enctype="multipart/form-data" target="iframe">
<input type="hidden" name="golive" value="0">
<div align="center">
	<input type="submit" value="Save Draft" class=" btn btn-primary">&nbsp;
	<input type="button" value="Discard Draft" class="discard  btn btn-warning">&nbsp;
	<input type="button" value="Go Live!" class="golive  btn btn-success">&nbsp;
	<input type="button" value="Expand All" class="expand  btn btn-info">&nbsp;
	<input type="button" value="Collapse All" class="collapse  btn btn-info">&nbsp;
</div>
<?php

extract($_REQUEST);
$condraft="draft.";
$filenamexs=preg_split("/\./",$xmlfile);
	if(file_exists("../scripts/draft.$xmlfile")):
		$xmlfile="draft.$xmlfile";	
	endif;
	$docxml= utf8_encode(file_get_contents("../scripts/".$xmlfile));
	$xml=simplexml_load_string($docxml);
	function cmp($a,$b){
		return strnatcmp(strtolower($a->keycode),strtolower($b->keycode));
	}
	
	foreach($xml[0]->nodes->node as $xmlnodes):
		$sortables[] = $xmlnodes;
	endforeach;
	usort($sortables,"cmp");
	$tind=0;
?>
	<h1>
		scripts/<?php echo $filenamexs[0];?>
		<div class="menuright">
		</div>
	</h1>	
	<input type="hidden" name="title" value="<?php echo $xml[0]->title?>">
	<input type="hidden" name="filetitle" value="<?php echo $filenamexs[0]?>">
	<?php foreach($sortables as $xmlnode):?>
	<h2 class="toptitle"><?php echo $xmlnode->keycode?></h2>
	<div class="scriptdetail"> 
	<table width="100%">	
	<tr>
	<td width="14%">Key Code:</td>
	<td width="86%"><input type="text" name="keycode[]" value="<?php echo $xmlnode->keycode;?>"></td>
	</tr>
	<tr>
	<td>Visible Text:</td><td><textarea name="visibletext[]"><?php echo $xmlnode->visibleText;?></textarea></td>
	</tr>
	<tr>
	<td>Invisible Text:</td><td><textarea name="invisibletext[]"><?php echo $xmlnode->invisibleText;?></textarea></td>
	</tr>
	<tr>
	<td>Audio File:</td><td><input name="audiofile[]" type="file"><br>
	<i>
		<?php echo $xmlnode->audioFile;?> -- Click here to play
		<audio style="display:none" src="/scriptengine/scripts/<?php 
	$leaudio=preg_split("/\//",$xmlnode->audioFile);
	if(file_exists("../scripts/".$leaudio[0]."/draft.$leaudio[1]")):
		echo $leaudio[0]."/draft".$leaudio[1];
	else:
		echo $xmlnode->audioFile;
	endif;
	?>"></audio>
	</i>
	<input type="hidden" name="xaudiofile[]" value="<?php echo $xmlnode->audioFile;?>">
	</td>
	</tr>
	<tr>
	<td colspan="2" align="left" class="adnode">
		<input type="button" value="Done" class="done btn btn-success">&nbsp;
		<input type="button" value="Delete" class="cancel btn btn-danger">
	</td>
	</tr>
	</table>
	<h2>Responses</h2><div class="h3_response_open">
	<?php 
		if(sizeof($xmlnode->responses->response)>0):
	 	foreach($xmlnode->responses->response as $response):
	?>
	<h3>
		<?php echo $response->keycode.". ".$response->visibleText;?>
		<div><input type="button" value="Edit" class="edit btn btn-primary"></div>
	</h3>
	<table width="100%" class="response_details detail">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="response_keycode[<?php echo $tind?>][]" value="<?php echo $response->keycode;?>"></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="response_visibletext[<?php echo $tind?>][]"><?php echo $response->visibleText;?></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Invisible Text:</td><td><textarea name="response_invisibletext[<?php echo $tind?>][]"><?php echo $response->invisibleText;?></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="response_audiofile[<?php echo $tind?>][]" type="file"><br>
		<i>
			<?php echo $response->audioFile;?> -- Click here to play
			<audio style="display:none" src="/scriptengine/scripts/<?php 
	$leaudio=preg_split("/\//",$response->audioFile);
	if(file_exists("../scripts/".$leaudio[0]."/draft.$leaudio[1]")):
		echo $leaudio[0]."/".$condraft.$leaudio[1];
	else:
		echo $response->audioFile;
	endif;
?>"></audio>
		</i>
		<input type="hidden" name="xresponse_audiofile[<?php echo $tind; ?>][]" value="<?php echo $response->audioFile;?>">
		</td>
	</tr>
	<tr>
		<td></td>	
		<td>Auto Goto:</td>
		<td><input type="text" name="response_autogoto[<?php echo $tind?>][]" value="<?php echo $response->autoGoto;?>"></td>
	<tr>
		<td colspan="3">
		<input type="button" value="Done" class="done btn btn-success">&nbsp;
		<input type="button" value="Delete" class="cancel btn btn-danger">
		</td>	
	</tr>
	</table>
	<?php 
		endforeach;
		endif;
	?>
	<div class="hidden_response_div hidden_div">
	<h3>
		New
		<div><input type="button" value="Edit" class="edit btn btn-info"></div>
	</h3>
	<table width="100%" class="hidden_response_details hidden_detail" style="display:block">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="hidden_response_keycode[<?php echo $tind?>][]" value=""></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="hidden_response_visibletext[<?php echo $tind?>][]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Invisible Text:</td><td><textarea name="hidden_response_invisibletext[<?php echo $tind?>][]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="hidden_response_audiofile[<?php echo $tind?>][]" type="file"><br>
		</td>
	</tr>
	<tr>
		<td></td>	
		<td>Auto Goto:</td>
		<td><input type="text" name="hidden_response_autogoto[<?php echo $tind?>][]" value=""></td>
	</tr>
	<tr>
	<td colspan="3" align="left">
		<input type="button" value="Add" class="okaysave btn btn-primary">&nbsp;
		<input type="button" value="Cancel" class="cancel btn btn-warning">
	</td>
	</tr>
	</table>
	</div>
	<div align="center"><input type="button" value="Add Response" class="add_new btn btn-primary"></div>
	</div>
	
	</div>	

	

<?php
	$tind++;
	endforeach;
?>

	<!--node-->
	<div class="hidden_response_div hidden_div">
	<h2 class="toptitle">New</h2>
	<div class="scriptdetail">
	<table width="100%">
	<tr>
		<td width="20%">Keycode:</td>
		<td width="80%"><input type="text" name="hidden_keycode[]" value=""></td>
	</tr>
	<tr>
		<td>Visible Text:</td><td><textarea name="hidden_visibletext[]"></textarea></td>
	</tr>
	<tr>
		<td>Invisible Text:</td><td><textarea name="hidden_invisibletext[]"></textarea></td>
	</tr>
	<tr>
		<td>Audio File:</td><td><input name="hidden_audiofile[]" type="file"><br>
		</td>
	</tr>
	<tr>
	<td colspan="2" align="left" class="adnode">
		<input type="button" value="Add" class="okaysave btn btn-primary">&nbsp;
		<input type="button" value="Cancel" class="cancel btn btn-warning">
	</td>
	</tr>
	</table>
	</div>
	</div>
	<!--node-->
	<div align="center" style="margin-bottom:7px;"><input type="button" value="Add Node" class="add_new"></div>
	
	<h2 class="toptitle">Rebuttal</h2>
		<div class="scriptdetail h3_response">
	<?php 
		if(sizeof($xml[0]->rebuttals->rebuttal)>0):
	 	foreach($xml[0]->rebuttals->rebuttal as $rebuttal):
	?>
	<h3>
		<?php echo $rebuttal->keycode.". ".$rebuttal->visibleText;?>
		<div><input type="button" value="Edit" class="edit"></div>
	</h3>
	<table width="100%" class="rebuttal_details detail">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="rebuttal_keycode[]" value="<?php echo $rebuttal->keycode;?>"></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="rebuttal_visibletext[]"><?php echo $rebuttal->visibleText;?></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Invisible Text:</td><td><textarea name="rebuttal_invisibletext[]"><?php echo $rebuttal->invisibleText;?></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="rebuttal_audiofile[]" type="file"><br>
		<i>
			<?php echo $rebuttal->audioFile;?> -- Click here to play
			<audio style="display:none" src="/scriptengine/scripts/<?php 
	$leaudio=preg_split("/\//",$rebuttal->audioFile);
	if(file_exists("../scripts/".$leaudio[0]."/draft.$leaudio[1]")):
		echo $leaudio[0]."/".$condraft.$leaudio[1];
	else:
		echo $rebuttal->audioFile;
	endif;
?>"></audio>
		</i>
		<input type="hidden" name="xrebuttal_audiofile[]" value="<?php echo $rebuttal->audioFile;?>">
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<input type="button" value="Delete" class="cancel btn btn-danger">
		</td>	
	</tr>
	</table>
	<?php 
		endforeach;
		endif;
	?>

	<div class="hidden_response_div hidden_div">
	<h3>
		New
		<div><input type="button" value="Edit" class="edit btn btn-info"></div>
	</h3>
	<table width="100%" class="hidden_rebuttal_details hidden_detail" style="display:block">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="hidden_rebuttal_keycode[]" value=""></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="hidden_rebuttal_visibletext[]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Invisible Text:</td><td><textarea name="hidden_rebuttal_invisibletext[]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="hidden_rebuttal_audiofile[]" type="file"><br>
		</td>
	</tr>
	<tr>
	<td colspan="3" align="left">
		<input type="button" value="Add" class="okaysave btn btn-primary">&nbsp;
		<input type="button" value="Cancel" class="cancel btn btn-warning">
	</td>
	</tr>
	</table>
	</div>

	<div align="center"><input type="button" value="Add Rebuttal" class="add_new btn btn-primary"></div>
	</div>

	<h2 class="toptitle">Hotkeys</h2>
	<div class="scriptdetail h3_response">
	<?php 
		if(sizeof($xml[0]->hotkeys->hotkey)>0):
		$haudioctr=0;
	 	foreach($xml[0]->hotkeys->hotkey as $hotkey):
	?>
	<h3>
		<?php echo $hotkey->keycode.". ".$hotkey->visibleText;?>
		<div><input type="button" value="Edit" class="edit"></div>
	</h3>
	<table width="100%" class="hotkey_details detail">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="hotkey_keycode[]" value="<?php echo $hotkey->keycode;?>"></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="hotkey_visibletext[]"><?php echo $hotkey->visibleText;?></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Auto Goto:</td>
		<td><input type="text" name="hotkey_autogoto[]" value="<?php echo $hotkey->autoGoto;?>"></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="hotkey_audiofile[]" type="file"><br>
	<?php 
		if(sizeof($hotkey->audioFiles)>0):
		foreach($hotkey->audioFiles->audioFile as $myaudio):
	?>
		<i>	
			<?php echo $myaudio;?> -- Click here to play
			<audio style="display:none" src="/scriptengine/scripts/<?php 
	$leaudio=preg_split("/\//",$myaudio);
	if(file_exists("../scripts/".$leaudio[0]."/draft.".$leaudio[1])):
		echo $leaudio[0]."/".$condraft.$leaudio[1];
	else:
		echo $myaudio;
	endif;
?>"></audio>
		</i>
		<input type="hidden" name="xhotkey_audiofile[<?php echo $haudioctr;?>][]" value="<?php echo $myaudio;?>"><br>
	<?php
		endforeach;
		else:
		if($hotkey->audioFile!=""):
	?>
		<i>	
			<?php echo $hotkey->audioFile;?> -- Click here to play
			<audio style="display:none" src="/scriptengine/scripts/<?php 
	$leaudio=preg_split("/\//",$hotkey->audioFile);
	if(file_exists("../scripts/".$leaudio[0]."/draft.".$leaudio[1])):
		echo $leaudio[0]."/".$condraft.$leaudio[1];
	else:
		echo $hotkey->audioFile;
	endif;
?>"></audio>
		</i>
		<input type="hidden" name="xhotkey_audiofile[<?php echo $haudioctr;?>][]" value="<?php echo $hotkey->audioFile;?>">
		<?php endif; ?>
		<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<input type="button" value="Delete" class="hkcancel btn btn-danger">
		</td>	
	</tr>
	</table>
	<?php 
		$haudioctr++;
		endforeach;
		endif;
	?>
	<div class="hidden_response_div hidden_div">
	<h3>
		New
		<div><input type="button" value="Edit" class="edit btn btn-primary"></div>
	</h3>
	<table width="100%" class="hidden_hotkey_details hidden_detail" style="display:block">
	<tr>
		<td width="10%"></td>	
		<td width="10%">Keycode:</td>
		<td width="80%"><input type="text" name="hidden_hotkey_keycode[]" value=""></td>
	</tr>
	<tr>
		<td></td>
		<td>Visible Text:</td><td><textarea name="hidden_hotkey_visibletext[]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Auto Goto:</td><td><textarea name="hidden_hotkey_autogoto[]"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>Audio File:</td><td><input name="hidden_hotkey_audiofile[]" type="file"><br>
		</td>
	</tr>
	<tr>
	<td colspan="3" align="left">
		<input type="button" value="Add" class="okaysave  btn btn-success">&nbsp;
		<input type="button" value="Cancel" class="cancel btn btn-warning">
	</td>
	</tr>
	</table>
	</div>
		<div align="center"><input type="button" value="Add Hotkey" class="add_new"></div>
		</div>

<div align="center">
	<input type="hidden" name="hksize" value="<?php echo $haudioctr?>">
	<input type="submit" value="Save Draft" class="btn btn-primary">&nbsp;
	<input type="button" value="Discard Draft" class="discard btn btn-warning">&nbsp;
	<input type="button" value="Go Live!" class="golive btn btn-success">&nbsp;
	<input type="button" value="Expand All" class="expand btn btn-info">&nbsp;
	<input type="button" value="Collapse All" class="collapse btn btn-info">&nbsp;
</div>

</form>
<div style="display:none">
	<iframe id="iframe" name="iframe"></iframe>
</div>
